(function ($) {

Drupal.behaviors.modalframeLogin = function() {
  var settings = Drupal.settings.modalframe_login;
  $(settings).each(function(index, setting) {

    $('a[href^="/' + setting.href + '"]:not(.modalframe-login-processed)').addClass('modalframe-login-processed').click(function() {
      var element = this;

      // This is our onSubmit callback that will be called from the child window
      // when it is requested by a call to modalframe_close_dialog() performed
      // from server-side submit handlers.
      function onSubmitCallback(args, statusMessages) {
        // Display status messages generated during submit processing.
        if (statusMessages) {
          $('.modalframe-login-messages').hide().html(statusMessages).show('slow');
        }

        if (args && args.message) {
          // Provide a simple feedback alert deferred a little.
          setTimeout(function() { alert(args.message); }, 500);
        }
        window.scrollTo(0, 0);
        setTimeout(function() { window.location.reload(); }, 100);
      }

      // Hide the messages are before opening a new dialog.
      $('.modalframe-login-messages').hide('fast');

      // Build modal frame options.
      var modalOptions = {
        url: $(element).attr('href'),
        autoFit: true,
        onSubmit: onSubmitCallback
      };

      // Obtain the width and height from our settings for this form.
      if (modalOptions.url.indexOf(setting.href) == 1) {
        modalOptions.width = parseInt(setting.width);
        modalOptions.height = parseInt(setting.height);
      }

      // Open the modal frame dialog.
      Drupal.modalFrame.open(modalOptions);

      // Prevent default action of the link click event.
      return false;
    });

  });

};

})(jQuery);
