<?php

/**
 * @file
 */

function modalframe_login_settings_form($form_state) {
  $form = array();
  $form_ids = modalframe_login_get_form_ids();
  foreach ($form_ids as $form_id) {
    $width = variable_get('modalframe_login_'. $form_id .'_width', NULL);
    $height = variable_get('modalframe_login_'. $form_id .'_height', NULL);
    $form['modalframe_login_'. $form_id] = array(
      '#type' => 'fieldset',
      '#title' => t('Form: !form', array('!form' => $form_id)),
      '#collapsible' => TRUE,
      '#collapsed' => ($width && $height),
    );
    $form['modalframe_login_'. $form_id]['modalframe_login_'. $form_id .'_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#description' => t("Set the width for this form's modalframe"),
      '#default_value' => $width,
    );
    $form['modalframe_login_'. $form_id]['modalframe_login_'. $form_id .'_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#description' => t("Set the height for this form's modalframe"),
      '#default_value' => $height,
    );
  }
  
  return system_settings_form($form);
}